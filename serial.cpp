#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include<vector>
#include <math.h>
#include "common.h"
using namespace std;
//
//  benchmarking program
//
int main( int argc, char **argv )
{    
    int navg,nabsavg=0;
    double davg,dmin, absmin=1.0, absavg=0.0;

    if( find_option( argc, argv, "-h" ) >= 0 )
    {
        printf( "Options:\n" );
        printf( "-h to see this help\n" );
        printf( "-n <int> to set the number of particles\n" );
        printf( "-o <filename> to specify the output file name\n" );
        printf( "-s <filename> to specify a summary file name\n" );
        printf( "-no turns off all correctness checks and particle output\n");
        return 0;
    }
    
    int n = read_int( argc, argv, "-n", 1000 );

    char *savename = read_string( argc, argv, "-o", NULL );
    char *sumname = read_string( argc, argv, "-s", NULL );
    
    FILE *fsave = savename ? fopen( savename, "w" ) : NULL;
    FILE *fsum = sumname ? fopen ( sumname, "a" ) : NULL;

    particle_t *particles = (particle_t*) malloc( n * sizeof(particle_t) );
    set_size( n );
    init_particles( n, particles );
    
    //------------------------------------------------------------------------
    double cutoff = 0.01;//from common
     
    
    
    //size is derived from particle density(in common)
    double sizing = sqrt(n * 0.0005);
    //length per group
    double grp_size = sizing / floor(sizing / cutoff);
    int numCells = pow((sizing / grp_size), 2);
    //groups per row
    int row = (int)sqrt(numCells);
    vector<vector<particle_t*> > groups(numCells);

    double simulation_time = read_timer( );
    for( int step = 0; step < NSTEPS; step++ )
    {
       
        navg = 0;
        davg = 0.0;
        dmin = 1.0;
        //
        //  compute forces
        //
        //fill groups with the addresses of particles
        for(int i = 0; i < n; i++){
            
            int x = floor(particles[i].x /grp_size);
            int y = floor(particles[i].y / grp_size);
            int index =x * row +y;
            groups[index].push_back(&particles[i]);
        }
       //go through every group
        for(int x = 0; x < row; x++){
            for(int y = 0; y < row; y++){
                int index = x * row + y;
                std::vector<particle_t*> currGroup = groups[index];
                //now go through every particle in group, set ax ay to zero
                for (int z = 0; z < currGroup.size(); z++){
                    currGroup[z]->ax = currGroup[z]->ay = 0;
                }
                //std::vector<particle_t*> currGroup = groups[index];
                //go through every neigboring group
                int nearX = x-1;
                int nearY = y-1;
                if(nearY >=0 && nearY <= row - 1){
                            if(nearX >=0 && nearX <= row - 1){
                                //get the index for the nearby group
                            int nearIndex = ((nearX + row) % row) * row + ((nearY + row) % row);
                           std::vector<particle_t*> nearGroup = groups[nearIndex];
                            //g through every particle and apply force
                            for(int z = 0; z < currGroup.size(); z++){
                                
                                for(int nearZ = 0; nearZ < nearGroup.size(); nearZ++){
                                        
                                        apply_force(*currGroup[z],*nearGroup[nearZ],&dmin,&davg,&navg);
                                    
                                }
                            }
                            }
                        }
                nearX = x;
                nearY = y-1;
              if(nearY >=0 && nearY <= row - 1){
                            if(nearX >=0 && nearX <= row - 1){
                                //get the index for the nearby group
                            int nearIndex = ((nearX + row) % row) * row + ((nearY + row) % row);
                           std::vector<particle_t*> nearGroup = groups[nearIndex];
                            //g through every particle and apply force
                            for(int z = 0; z < currGroup.size(); z++){
                                
                                for(int nearZ = 0; nearZ < nearGroup.size(); nearZ++){
                                        
                                        apply_force(*currGroup[z],*nearGroup[nearZ],&dmin,&davg,&navg);
                                    
                                }
                            }
                            }
                        }
                
                nearX = x+1;
                nearY = y-1;
                if(nearY >=0 && nearY <= row - 1){
                            if(nearX >=0 && nearX <= row - 1){
                                //get the index for the nearby group
                            int nearIndex = ((nearX + row) % row) * row + ((nearY + row) % row);
                           std::vector<particle_t*> nearGroup = groups[nearIndex];
                            //g through every particle and apply force
                            for(int z = 0; z < currGroup.size(); z++){
                                
                                for(int nearZ = 0; nearZ < nearGroup.size(); nearZ++){
                                        
                                        apply_force(*currGroup[z],*nearGroup[nearZ],&dmin,&davg,&navg);
                                    
                                }
                            }
                            }
                        }
                
                nearX = x+1;
                nearY = y;
               if(nearY >=0 && nearY <= row - 1){
                            if(nearX >=0 && nearX <= row - 1){
                                //get the index for the nearby group
                            int nearIndex = ((nearX + row) % row) * row + ((nearY + row) % row);
                           std::vector<particle_t*> nearGroup = groups[nearIndex];
                            //g through every particle and apply force
                            for(int z = 0; z < currGroup.size(); z++){
                                
                                for(int nearZ = 0; nearZ < nearGroup.size(); nearZ++){
                                        
                                        apply_force(*currGroup[z],*nearGroup[nearZ],&dmin,&davg,&navg);
                                    
                                }
                            }
                            }
                        }
                
                nearX = x+1;
                nearY = y+1;
                if(nearY >=0 && nearY <= row - 1){
                            if(nearX >=0 && nearX <= row - 1){
                                //get the index for the nearby group
                            int nearIndex = ((nearX + row) % row) * row + ((nearY + row) % row);
                           std::vector<particle_t*> nearGroup = groups[nearIndex];
                            //g through every particle and apply force
                            for(int z = 0; z < currGroup.size(); z++){
                                
                                for(int nearZ = 0; nearZ < nearGroup.size(); nearZ++){
                                        
                                        apply_force(*currGroup[z],*nearGroup[nearZ],&dmin,&davg,&navg);
                                    
                                }
                            }
                            }
                        }
                
                nearX = x;
                nearY = y+1;
                if(nearY >=0 && nearY <= row - 1){
                            if(nearX >=0 && nearX <= row - 1){
                                //get the index for the nearby group
                            int nearIndex = ((nearX + row) % row) * row + ((nearY + row) % row);
                           std::vector<particle_t*> nearGroup = groups[nearIndex];
                            //g through every particle and apply force
                            for(int z = 0; z < currGroup.size(); z++){
                                
                                for(int nearZ = 0; nearZ < nearGroup.size(); nearZ++){
                                        
                                        apply_force(*currGroup[z],*nearGroup[nearZ],&dmin,&davg,&navg);
                                    
                                }
                            }
                            }
                        }
                
                nearX = x-1;
                nearY = y+1;
                if(nearY >=0 && nearY <= row - 1){
                            if(nearX >=0 && nearX <= row - 1){
                                //get the index for the nearby group
                            int nearIndex = ((nearX + row) % row) * row + ((nearY + row) % row);
                           std::vector<particle_t*> nearGroup = groups[nearIndex];
                            //g through every particle and apply force
                            for(int z = 0; z < currGroup.size(); z++){
                                
                                for(int nearZ = 0; nearZ < nearGroup.size(); nearZ++){
                                        
                                        apply_force(*currGroup[z],*nearGroup[nearZ],&dmin,&davg,&navg);
                                    
                                }
                            }
                            }
                        }
                
                nearX = x-1;
                nearY = y;
                if(nearY >=0 && nearY <= row - 1){
                            if(nearX >=0 && nearX <= row - 1){
                                //get the index for the nearby group
                            int nearIndex = ((nearX + row) % row) * row + ((nearY + row) % row);
                           std::vector<particle_t*> nearGroup = groups[nearIndex];
                            //g through every particle and apply force
                            for(int z = 0; z < currGroup.size(); z++){
                                
                                for(int nearZ = 0; nearZ < nearGroup.size(); nearZ++){
                                        
                                        apply_force(*currGroup[z],*nearGroup[nearZ],&dmin,&davg,&navg);
                                    
                                }
                            }
                            }
                        }
                        
                nearX = x;
                nearY = y;
                if(nearY >=0 && nearY <= row - 1){
                            if(nearX >=0 && nearX <= row - 1){
                                //get the index for the nearby group
                            int nearIndex = ((nearX + row) % row) * row + ((nearY + row) % row);
                           std::vector<particle_t*> nearGroup = groups[nearIndex];
                            //g through every particle and apply force
                            for(int z = 0; z < currGroup.size(); z++){
                                
                                for(int nearZ = 0; nearZ < nearGroup.size(); nearZ++){
                                        
                                        apply_force(*currGroup[z],*nearGroup[nearZ],&dmin,&davg,&navg);
                                    
                                }
                            }
                            }
                        }
                
                
            }
        }
        
        
        
 //--------------------------------------------------------------------------------------       
        
        for( int i = 0; i < n; i++ ){
            move(particles[i]);
        } 
        
        //setting the groups clear to fill with particles as determined by their new positions after being moved
        for(int i = 0; i < numCells; i++){
            groups[i].clear();
        }
            
        

        if( find_option( argc, argv, "-no" ) == -1 )
        {
          if (navg) {
            absavg +=  davg/navg;
            nabsavg++;
          }
          if (dmin < absmin) absmin = dmin;
		
          if( fsave && (step%SAVEFREQ) == 0 )
              save( fsave, n, particles );
        }
    }
    simulation_time = read_timer( ) - simulation_time;
    
    printf( "n = %d, simulation time = %g seconds", n, simulation_time);

    if( find_option( argc, argv, "-no" ) == -1 )
    {
      if (nabsavg) absavg /= nabsavg;
    // 
    //  -the minimum distance absmin between 2 particles during the run of the simulation
    //  -A Correct simulation will have particles stay at greater than 0.4 (of cutoff) with typical values between .7-.8
    //  -A simulation were particles don't interact correctly will be less than 0.4 (of cutoff) with typical values between .01-.05
    //
    //  -The average distance absavg is ~.95 when most particles are interacting correctly and ~.66 when no particles are interacting
    //
    printf( ", absmin = %lf, absavg = %lf", absmin, absavg);
    if (absmin < 0.4) printf ("\nThe minimum distance is below 0.4 meaning that some particle is not interacting");
    if (absavg < 0.8) printf ("\nThe average distance is below 0.8 meaning that most particles are not interacting");
    }
    printf("\n");     

    if( fsum) 
        fprintf(fsum,"%d %g\n",n,simulation_time);
 
    if( fsum )
        fclose( fsum );    
    free( particles );
    if( fsave )
        fclose( fsave );
    
    return 0;
}
